variable "name" {
    default = "skygate"
}

variable "region" {
    default = "us-east-1"
}

variable "access_key" {
    default = ""
}

variable "secret_key" {
    default = ""
}

variable "az" {
    default = "us-east-2a"
}

variable "ssh_key_name" {
    default = ""
}

variable "vpc_cidr_block" {
    default = ""
}

variable "dmz_cidr_block" {
    default = ""
}

variable "main_instance_private_ip" {
    default = "10.136.1.39"
}

